from django.contrib import admin
# from .models import Registration
# from .models import admin_add
from .models import *
# Register your models here.

admin.site.register(Registration)
admin.site.register(admin_add)
admin.site.register(Department)
admin.site.register(Category)
admin.site.register(SubCategory)
admin.site.register(PO)
admin.site.register(IVR)
admin.site.register(Employee)
