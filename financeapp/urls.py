
from django.urls import path 
from financeapp import views


urlpatterns = [
    path("",views.login, name=""),
    path("login/",views.login,name="login"),
    path('logout',views.log_out,name='logout'),
    path('home',views.afterlogin , name='home'),
    path('admin_dashboard',views.admin_dashboard,name='admin_dashboard'),
    path('presidentHome',views.presidentHome, name='presidentHome'),
    path('hodhome',views.hodHome,name='hodhome'),
    path('staffhome',views.staffHome,name='staffhome'),
    # path('hofhome',views.hofHome,name='hofhome'),
    path('fteamhome',views.fteamHome,name='fteamhome'),
    # path('profile',views.profile,name='profile'),
    path('forgotpassword',views.forgotpassword,name='forgotpassword'),
    # path('admin_add.html',views.admin_add1,name='admin_add.html'),
    # path('admindashboard',views.displayUser,name='admindashboard')

    path('admin_profile',views.admin_profile,name='admin_profile'),
    path('admin_dashboard_profile',views.admin_dashboard_profile,name='admin_dashboard_profile'),
    path('admin_edit_profile',views.admin_edit_profile,name='admin_edit_profile'),
    path('hod_profile',views.hod_profile,name='hod_profile'),
    path('staff_dashboard_profile',views.staff_dashboard_profile,name="staff_profile"),
    path('staff_submit_po_profile',views.staff_submit_po_profile,name='staff_submit_po_profile'),
    path('staff_submit_ivr_profile',views.staff_submit_ivr_profile,name="staff_submit_ivr_profile"),
    path('hod_submit_profile',views.hod_submit_profile,name='hod_submit_profile'),
    path('hod_approval_profile',views.hod_approval_profile,name='hod_approval_profile'),
    path('president_profile',views.president_dashboard_profile,name='president_profile'),
    path('president_po_for_approval_profile',views.president_po_for_approval_profile,name='president_po_for_approval_profile'),
    path('president_ivr_for_approval_profile',views.president_ivr_for_approval_profile,name='president_ivr_for_approval_profile'),
    path('finance_profile',views.finanace_team_profile,name='finance_profile'),
    path('hof_profile',views.hof_profile,name='hof_profile'),

    path('admin_add/',views.admin_add2, name='admin_add'),
    path('admin_edit/',views.admin_edit, name='admin_edit'),
    path('admin_edit_user/', views.admin_edit_user, name='admin_edit_user'),
    path('admin_edit_cat/', views.admin_edit_cat, name='admin_edit_cat'),
    path('admin_edit_subcat/', views.admin_edit_subcat, name='admin_edit_subcat'),
    path('admin_edit_depart/', views.admin_edit_department, name='admin_edit_depart'),

    path('hod_view/', views.hod_view, name='hod_view'),
    path('hod_po_submitted_list/', views.hod_po_submitted_list, name='hod_po_submitted_list'),
    path('hod_ivr_submitted_list/', views.hod_ivr_submitted_list, name='hod_ivr_submitted_list'),
    path('hod_ivr/', views.hod_ivr, name='hod_ivr'),
    path('hod_po/', views.hod_po, name='hod_po'),
    path('hod_approval/', views.hod_approval, name='hod_approval'),
    path('hod_submit/', views.hod_submit, name='hod_submit'),

    path('staff_dashboard/', views.staff_dashboard, name='staff_dashboard'),
    path('staff_po_list/', views.staff_po_list, name='staff_po_list'),
    path('staff_ivr_list/', views.staff_ivr_list, name='staff_ivr_list'),
    path('staff_submit_po/', views.staff_submit_po, name='staff_submit_po'),
    path('staff_submit_ivr/', views.staff_submit_ivr, name='staff_submit_ivr'),

    path('president_dashboard/', views.president_dashboard, name='president_dashboard'),
    path('president_dashboard_po/', views.president_dashboard_po, name='president_dashboard_po'),
    path('president_dashboard_ivr/', views.president_dashboard_ivr, name='president_dashboard_ivr'),
    path('president_po_for_approval/', views.president_po_for_approval, name='president_po_for_approval'),
    path('president_ivr_for_approval/', views.president_ivr_for_approval, name='president_ivr_for_approval'),

    path('ft_view/', views.ft_view, name='ft_view'),
    path('ft_po_submitted_list/', views.ft_po_submitted_list, name='ft_po_submitted_list'),
    path('ft_ivr_submitted_list/', views.ft_ivr_submitted_list, name='ft_ivr_submitted_list'),
    path('ft_ivr/', views.ft_ivr, name='ft_ivr'),
    path('ft_po/', views.ft_po, name='ft_po'),
    path('ft_approval/', views.ft_approval, name='ft_approval'),
    path('ft_submit/', views.ft_submit, name='ft_submit'),

    path('hof_dashboard/', views.hof_dashboard, name='hof_dashboard'),
    path('hof_dashboard_ivr/', views.hof_dashboard_ivr, name='hof_dashboard_ivr'),
    path('hof_dashboard_po/', views.hof_dashboard_po, name='hof_dashboard_po'),
    path('hof_ivr_for_approval/', views.hof_ivr_for_approval, name='hof_ivr_for_approval'),
    path('hof_po_for_approval/', views.hof_po_for_approval, name='hof_po_for_approval'),
    path('hof_pending_forms/', views.hof_pending_forms, name='hof_pending_forms'),
    path('hof_pending_ivr/', views.hof_pending_ivr, name='hof_pending_ivr'),
    path('hof_pending_po/', views.hof_pending_po, name='hof_pending_po'),

    # path('po/', views.PO_Form_Page, name='po_form'),
    # path('ivr/', views.IVR_Form_Page, name='ivr_form'),
    path('download/<str:pno>', views.download_file, name='download_file'),

    path('export_po/<str:po_no>/', views.export_po, name='export_po'),
    path('export_ivr/<str:inv_no>/', views.export_ivr, name='export_ivr'),
    path('export_selected_po/', views.export_selected_po, name='export_selected_po'),
    path('export_selected_ivr/', views.export_selected_ivr, name='export_selected_ivr'),
    path('hof_approval/', views.hof_approval, name='hof_approval'),

    # path('download/<str:pno>', download_file, name='download_file'),


]
