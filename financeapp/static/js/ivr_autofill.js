document.getElementById('po_no').addEventListener('change',
function() {
  var po_no = this.value;
  fetch('/get_po_data/' + po_no)
  .then(response => response.json())
  .then(data => {
    document.getElementById("department").value = data.department;
    document.getElementById("category").value = data.category;
    document.getElementById("sub_category").value = data.sub_category;
    document.getElementById("amount_currency").value = data.amount_currency;
    document.getElementById("currency_show").innerText = '('+data.currency+')';
    document.getElementById("currency").value = data.currency;
    document.getElementById("amount").value = data.amount;
    document.getElementById("supplier_name").value = data.supplier_name;
    document.getElementById("description").value = data.description;
    document.getElementById("quantity").value = data.quantity;
    document.getElementById("hod").value = data.hod;


  })
})