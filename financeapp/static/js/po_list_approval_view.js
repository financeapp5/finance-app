const rButtons = document.querySelectorAll('.reject_po');
  
rButtons.forEach((button) => {
  button.addEventListener('click',
  () => {
    const po_no = button.getAttribute('data-id');
    //find the row by its id
    document.getElementById('po_no_r').value = po_no;
    document.getElementById('id_r').value = po_no;
  })
})

const pButtons = document.querySelectorAll('.pending_po');
  
pButtons.forEach((button) => {
  button.addEventListener('click',
  () => {
    const po_no = button.getAttribute('data-id');
    //find the row by its id
    document.getElementById('po_no_p').value = po_no;
    document.getElementById('id_p').value = po_no;
  })
})
const viewButtons = document.querySelectorAll('.view_btn');
viewButtons.forEach((button) => {
  button.addEventListener('click',
  () => {
    const po_no = button.getAttribute('data-id');
    //find the row by its id
    const row = document.querySelector(`#table tr[data-id='${po_no}']`);
    
    //for view reject and approve btn
    document.getElementById('po_no_p').value = po_no;
    document.getElementById('id_p').value = po_no;

    document.getElementById('po_no_r').value = po_no;
    document.getElementById('id_r').value = po_no;
    
    document.querySelector('#po_no_view').innerText = row.querySelector('.po_no').innerText;
    document.querySelector('#email_submitted_by_view').innerText = row.querySelector('.email_submitted_by').innerText;
    document.querySelector('#category_view').innerText = row.querySelector('.category').innerText;
    document.querySelector('#amount_view').innerText = row.querySelector('.amount').innerText;
    document.querySelector('#department_view').innerText = row.querySelector('.department').innerText;
    document.querySelector('#date_view').innerText = row.querySelector('.date').innerText;
    document.querySelector('#subcategory_view').innerText = row.querySelector('.sub_category').innerText;
    document.querySelector('#hod_view').innerText = row.querySelector('.hod').innerText;
    document.querySelector('#currency_view').innerText = row.querySelector('.currency').innerText;
    document.querySelector('#amount_currency_view').innerText = row.querySelector('.amount_currency').innerText;
    document.querySelector('#quantity_view').innerText = row.querySelector('.quantity').innerText;
    
    document.querySelector('#desc_view').innerText = row.querySelector('.description').innerText;
    document.querySelector('#supplier_name_view').innerText = row.querySelector('.supplier_name').innerText;
    document.querySelector('#name_view').innerText = row.querySelector('.name').innerText;
    const td = row.querySelector('#attachement_url');

    const text = row.querySelector('.attachment').innerText;
    const parts = text.split('/');
    const fileName = parts[parts.length - 1];

    const a = document.createElement('a');
    a.href = td.getAttribute('data-url');
    a.download = fileName;
    a.style.textDecoration = 'none';
    // a.style.color = 'initial';
    const parts2 = fileName.split('.');
    const extension = parts2[parts2.length - 1];

    if (extension === 'pdf'){
      a.innerHTML = '<i class="fa-solid fa-file-pdf" style="font-size: 40px;color: red;"></i><br>Download PDF'
    }else if (extension === 'docx'){
      a.innerHTML = '<i class="fa-solid fa-file-word" style="font-size: 40px;color: #4392f1"></i><br>Download DOC'
    }else if (extension === 'xlsx'){
      a.innerHTML = '<i class="fa-solid fa-file-excel" style="font-size: 40px;color: green;"></i><br>Download XLS'
    }else if (extension === 'png' || extension === 'jpg'){
      a.innerHTML = '<i class="fa-solid fa-file-image" style="font-size: 40px;color: #000000"></i><br>Download Image'
    }else{
      a.innerHTML = '<i class="fa fa-file" style="font-size: 40px;color: #000000"></i><br>Download File'
    }
    document.getElementById('attachment_view').innerHTML='';
    document.getElementById('attachment_view').append(a);
  })
})