const vButtons = document.querySelectorAll('.view_btn');
  
vButtons.forEach((button) => {
  button.addEventListener('click',
  () => {
    const rowId = button.getAttribute('data-id');
    //find the row by its id
    const row = document.querySelector(`#table tr[data-id='${rowId}']`);
    
    const export_xls = row.querySelector('#export_an_excel').getAttribute('data-url');
    const view_export_btn = document.getElementById('view_export_btn');
    view_export_btn.addEventListener('click',()=>{
      const link = document.createElement('a');
      link.href = export_xls
      link.download = `${row}.xlsx`;
      link.click();
    })

    document.querySelector('#po_no_view').innerText = row.querySelector('.po_no').innerText;
    document.querySelector('#email_submitted_by_view').innerText = row.querySelector('.email_submitted_by').innerText;
    document.querySelector('#category_view').innerText = row.querySelector('.category').innerText;
    document.querySelector('#amount_view').innerText = row.querySelector('.amount').innerText;
    document.querySelector('#department_view').innerText = row.querySelector('.department').innerText;
    document.querySelector('#date_view').innerText = row.querySelector('.date').innerText;
    document.querySelector('#subcategory_view').innerText = row.querySelector('.sub_category').innerText;
    document.querySelector('#hod_view').innerText = row.querySelector('.hod').innerText;
    document.querySelector('#currency_view').innerText = row.querySelector('.currency').innerText;
    document.querySelector('#amount_currency_view').innerText = row.querySelector('.amount_currency').innerText;
    document.querySelector('#quantity_view').innerText = row.querySelector('.quantity').innerText;
    document.querySelector('#desc_view').innerText = row.querySelector('.description').innerText;
    document.querySelector('#supplier_name_view').innerText = row.querySelector('.supplier_name').innerText;
    document.querySelector('#name_view').innerText = row.querySelector('.name').innerText;

    document.querySelector('#payee_name_view').innerText = row.querySelector('.payee_name').innerText;
    document.querySelector('#payee_acc_no_view').innerText = row.querySelector('.payee_acc_no').innerText;
    document.querySelector('#tpn_no_view').innerText = row.querySelector('.tpn_no').innerText;
    document.querySelector('#payee_bank_name_view').innerText = row.querySelector('.payee_bank_name').innerText;
    
    const td = row.querySelector('#attachement_url');

    const text = row.querySelector('.attachment').innerText;
    const parts = text.split('/');
    const fileName = parts[parts.length - 1];

    const a = document.createElement('a');
    a.href = td.getAttribute('data-url');
    a.download = fileName;
    a.style.textDecoration = 'none';
    // a.style.color = 'initial';
    const parts2 = fileName.split('.');
    const extension = parts2[parts2.length - 1];

    if (extension === 'pdf'){
      a.innerHTML = '<i class="fa-solid fa-file-pdf" style="font-size: 40px;color: red;"></i><br>Download PDF'
    }else if (extension === 'docx'){
      a.innerHTML = '<i class="fa-solid fa-file-word" style="font-size: 40px;color: #4392f1"></i><br>Download DOC'
    }else if (extension === 'xlsx'){
      a.innerHTML = '<i class="fa-solid fa-file-excel" style="font-size: 40px;color: green;"></i><br>Download XLS'
    }else if (extension === 'png' || extension === 'jpg'){
      a.innerHTML = '<i class="fa-solid fa-file-image" style="font-size: 40px;color: #000000"></i><br>Download Image'
    }else{
      a.innerHTML = '<i class="fa fa-file" style="font-size: 40px;color: #000000"></i><br>Download File'
    }
    document.getElementById('attachment_view').innerHTML='';
    document.getElementById('attachment_view').append(a);
  })
})

const csrf_token = document.getElementsByName('csrfmiddlewaretoken')[0].value;

  const exportExcelButton = document.getElementById("export-excel-button");
  exportExcelButton.addEventListener("click", function(event) {
    event.preventDefault();

    // Get all the selected PO numbers
    const checkboxes = document.querySelectorAll("input[name='selected_po']");
    const selectedPOs = [];

    const select_all = document.querySelector("input[name='select_all']");
    if (select_all.checked){
      // Get all the elements that contain the PO number
      const po_numbers_element = document.querySelectorAll(".inv_no");

      // Loop through the elements and extract the PO number
      po_numbers_element.forEach(function(element) {
        selectedPOs.push(element.textContent);
      });
      console.log(selectedPOs)
    }else{
      // Loop through the checkboxes and push the selected PO numbers to the array
      checkboxes.forEach(function(checkbox) {
      if (checkbox.checked) {
          selectedPOs.push(checkbox.value);
        }
      });
    }

    // Send a POST request to the server to export to Excel
    fetch('/export_selected_ivr/', {
      method: 'POST',
      body: JSON.stringify({selected_po: selectedPOs, export_format: 'excel'}),
      headers: {
        'Content-Type': 'application/json',
        'X-CSRFToken': csrf_token
      }
    })
    .then(function(response) {
      // Handle the server response
      console.log(response)

      return response.blob();
    })
    .then(function(data) {
      // Create a link element and set its href to the URL of the excel file
      const link = document.createElement('a');
      link.href = URL.createObjectURL(data);
      link.download = 'selected_pos.xlsx';
      link.click();
    })
    .catch(function(error) {
      // Handle any errors
      console.error(error);
    });
  });
  
//this is for the select all checkbox
  const selectAllCheckbox = document.getElementById("select_all");
  const poCheckboxes = document.querySelectorAll("input[name='selected_po']");

  selectAllCheckbox.addEventListener("change", function(event) {
    // Check or uncheck all the PO checkboxes
    poCheckboxes.forEach(function(checkbox) {
      checkbox.checked = selectAllCheckbox.checked;
    });
  });

  poCheckboxes.forEach(function(checkbox) {
    checkbox.addEventListener("change", function(event) {
        let allChecked = true;
        poCheckboxes.forEach(function(cb) {
            if (!cb.checked) {
                allChecked = false;
            }
        });
        selectAllCheckbox.checked = allChecked;
    });
});