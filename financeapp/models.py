from django.db import models
import datetime
from django.db.models.signals import post_save
from django.dispatch import receiver
# Create your models here.
# class Registration(models.Model):
#     name = models.CharField(max_length=100)
#     temp_password= models.CharField(max_length=100)
#     email=models.EmailField()
#     role=models.CharField(max_length=200)
#     department=models.CharField(max_length=200)

class admin_add(models.Model):
    name =models.CharField(max_length=100)
    email=models.EmailField()
    employeeid=models.IntegerField()
    role=models.CharField(max_length=200)
    department=models.CharField(max_length=200)
    temp_password=models.CharField(max_length=100)
year = datetime.date.today().strftime('%Y')

class Employee(models.Model):
    first_name =models.CharField(max_length=100)
    last_name =models.CharField(max_length=100)
    email=models.EmailField()
    employee_id=models.IntegerField()
    role=models.CharField(max_length=200)
    department=models.CharField(max_length=200)
    temp_password=models.CharField(max_length=100)

year = datetime.date.today().strftime('%Y')

def POYYYY00001():
    largest = PO.objects.all().order_by('po_no').last()
    if not largest:
        pid = f'{1:05}'
        return 'PO'+year+pid
    
    lpo = largest.po_no
    po = 'PO'+year
    lpo = int(lpo.replace(po,'').lstrip())+1
    pid = f'{lpo:05}'
    return 'PO'+year+pid

def INVYYYY00001():
    largest = IVR.objects.all().order_by('inv_no').last()
    if not largest:
        inid = f'{1:05}'
        return 'IVR'+year+inid

    else:
        lpo = largest.inv_no
        po = 'IVR'+year
        lpo = int(lpo.replace(po,'').lstrip())+1
        inid = f'{lpo:05}'
        return 'IVR'+year+inid

class PO(models.Model):
    date = models.CharField(
        max_length=11,
        default=datetime.date.today().strftime('%d/%m/%Y'),
        editable=False
        )
    po_no = models.CharField(
        primary_key=True,
        max_length=20,
        default=POYYYY00001,
        editable=False
        )
    category = models.CharField(max_length=100,)
    sub_category = models.CharField(max_length=100)
    department = models.CharField(max_length=100,)
    hod = models.CharField(max_length=100)
    currency = models.CharField(max_length=20)
    amount_currency = models.IntegerField()
    amount = models.IntegerField()
    quantity = models.CharField(max_length=20)
    description = models.TextField()
    supplier_name = models.CharField(max_length=100)
    email_submitted_by = models.EmailField(max_length=100, blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    attachment = models.FileField(upload_to='uploads/%Y/%m/%d/')

    def __str__(self):
        return self.po_no

class Department(models.Model):
    department = models.CharField(max_length=100)
    hod = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return self.department

class Category(models.Model):
    category_name = models.CharField(max_length=100)
    description = models.TextField()

class SubCategory(models.Model):
    sub_category_name = models.CharField(max_length=100)
    description = models.TextField()
    category_name = models.CharField(max_length=100)

class IVR(models.Model):
    date = models.CharField(
        max_length=11,
        default=datetime.date.today().strftime('%d/%m/%Y'),
        editable=False
    )
    inv_no = models.CharField(
       primary_key=True,
        max_length=20,
        default=INVYYYY00001,
        editable=False,
        unique=True,
    )
    po_no = models.CharField(max_length=20,editable=True)
    category = models.CharField(max_length=100,)
    sub_category = models.CharField(max_length=100)
    department = models.CharField(max_length=100,)
    hod = models.CharField(max_length=100,)
    currency = models.CharField(max_length=20,)
    amount_currency = models.IntegerField()
    amount = models.IntegerField()
    quantity = models.CharField(max_length=20,)
    description = models.TextField()
    supplier_name = models.CharField(max_length=100,)
    email_submitted_by = models.EmailField(max_length=100)
    name = models.CharField(max_length=100, blank=True, null=True)
    attachment = models.FileField(upload_to='uploads/%Y/%m/%d/')
    payee_name = models.CharField(max_length=50)
    payee_acc_no = models.CharField(max_length=50)
    tpn_no = models.CharField(max_length=50) 
    payee_bank_name = models.CharField(max_length=255)

    def __str__(self):
        return self.inv_no

class Registration(models.Model):
    name = models.CharField(max_length=100)
    temp_password= models.CharField(max_length=100)
    email=models.EmailField()
    role=models.CharField(max_length=200)
    department=models.CharField(max_length=200)

class Employee(models.Model):
    first_name =models.CharField(max_length=100)
    last_name =models.CharField(max_length=100)
    email=models.EmailField()
    employee_id=models.IntegerField()
    role=models.CharField(max_length=200)
    department=models.CharField(max_length=200)
    temp_password=models.CharField(max_length=100)

#add to approval everytime po is submitted
@receiver(post_save, sender=PO)
def create_approval_instance(sender, instance, created, **kwargs):
    if created:
        Approval.objects.create(po_no=instance)

#status - means not sent for approval
#p pending, r rejected and a approved
#b means two waiting for other one
class Approval(models.Model):
    po_no = models.ForeignKey(PO, 
        on_delete=models.CASCADE, db_column='po_no')
    status = models.CharField(default='-', max_length=1)

#add to approval2 everytime ivr is submitted
@receiver(post_save, sender=IVR)
def create_approval2_instance(sender, instance, created, **kwargs):
    if created:
        Approval2.objects.create(inv_no=instance)
        
class Approval2(models.Model):
    inv_no = models.ForeignKey(IVR, 
        on_delete=models.CASCADE, db_column='inv_no')
    status = models.CharField(default='p', max_length=1)



