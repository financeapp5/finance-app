from django import forms
from financeapp.models import Registration
from .models import *

class PO_Form(forms.ModelForm):
    class Meta:
        model = PO
        fields = '__all__'

    #submit the current user email while submitting the form
    def save(self, commit=True, request=None):
        instance = super().save(commit=False)
        if request:
            email = request.session.get('email') or "default@email.com"
            instance.email_submitted_by = email
            name = request.session.get('first_name')+' '+request.session.get('last_name')
            instance.name = name
        if commit:
            instance.save()
        return instance

class DEPARTMENT_FORM(forms.ModelForm):

    class Meta:
        model = Department
        fields = '__all__'

class CATEGORY_FORM(forms.ModelForm):

    class Meta:
        model = Category
        fields = '__all__'

class SUB_CATEGORY_FORM(forms.ModelForm):

    class Meta:
        model = SubCategory
        fields = '__all__'

class IVR_FORM(forms.ModelForm):

    class Meta:
        model = IVR
        fields = '__all__'

    #submit the current user email and name while submitting the form
    def save(self, commit=True, request=None):
        instance = super().save(commit=False)
        if request:
            email = request.session.get('email')
            instance.email_submitted_by = email
            name = request.session.get('first_name')+' '+request.session.get('last_name')
            instance.name = name
        if commit:
            instance.save()
        return instance

class PO_INFO_INPUT_FORM(forms.Form):
    
    user_pno = forms.CharField(max_length=100,
        widget=forms.Select(attrs={'onchange': 'submit();'})
        )

class EMPLOYEE_FORM(forms.ModelForm):
    class Meta:
        model = Employee
        fields = '__all__'

class CATEGORY_DELETE_FORM(forms.Form):
    id = forms.IntegerField(required=True)

class SUBCATEGORY_DELETE_FORM(forms.Form):
    id = forms.IntegerField(required=True)

class DEPARTMENT_DELETE_FORM(forms.Form):
    id = forms.IntegerField(required=True)

class EMPLOYEE_DELETE_FORM(forms.Form):
    id = forms.IntegerField(required=True)

class PO_DELETE_FORM(forms.Form):
    id = forms.IntegerField(required=True)
    
class APPROVAL_FORM(forms.ModelForm):
    class Meta:
        model = Approval
        fields = '__all__'

    # def clean_status(self):
    #     status = self.cleaned_data['status']
    #     if status not in ['a', 'r']:
    #         raise forms.ValidationError('Invalid status. Only a or r allowed.')
    #     return status

    # def save(self, po_no, status, commit=True):
    #     # if the amount is gt 300k status will set to b, waiting for other' approval
    #     po_amount = PO.objects.get(po_no=po_no).amount
    #     instance = Approval.objects.get(po_no=po_no)
    #     status_from_instance = instance.status
    #     if status_from_instance == 'p':
    #         if po_amount > 300000 and status == 'a':
    #             instance.status = 'b'
    #         elif po_amount > 300000 and status == 'r':
    #             instance.status = 'r'
    #         elif  po_amount <= 300000 and status == 'a':
    #             instance.status = 'a'
    #         elif po_amount > 300000 and status == 'r':
    #             instance.status = 'r'
    #     elif status_from_instance == 'b':
    #         if po_amount > 300000 and status == 'a':
    #             instance.status = 'a'
    #         if po_amount > 300000 and status == 'r':
    #             instance.status = 'r'
    #     if commit:
    #         PO.objects.update_or_create(po_no=instance.po_no, defaults={'amount': po_amount})
    #         instance.save()
    #     return instance

class APPROVAL_FORM2(forms.ModelForm):
    class Meta:
        model = Approval2
        fields = '__all__' 

    # def clean_status(self):
    #     status = self.cleaned_data['status']
    #     if status not in ['a', 'r']:
    #         raise forms.ValidationError('Invalid status. Only a or r allowed.')
    #     return status

    # # if the amount is gt 300k status will set to b, waiting for other' approval
    # def save(self, inv_no, status, commit=True):
    #     ivr_amount = IVR.objects.get(inv_no=inv_no).amount
    #     instance = Approval2.objects.get(inv_no=inv_no)
    #     status_from_instance = instance.status
    #     if status_from_instance == 'p':
    #         if ivr_amount > 300000 and status == 'a':
    #             instance.status = 'b'
    #         elif ivr_amount > 300000 and status == 'r':
    #             instance.status = 'r'
    #         elif  ivr_amount <= 300000 and status == 'a':
    #             instance.status = 'a'
    #         elif ivr_amount > 300000 and status == 'r':
    #             instance.status = 'r'
    #     elif status_from_instance == 'b':
    #         if ivr_amount > 300000 and status == 'a':
    #             instance.status = 'a'
    #         if ivr_amount > 300000 and status == 'r':
    #             instance.status = 'r'
    #     if commit:
    #         IVR.objects.update_or_create(inv_no=instance.inv_no, defaults={'amount': ivr_amount})
    #         instance.save()
    #     return instance

class SEARCH_FORM(forms.Form):
    po_no = forms.CharField(label='PO Number', max_length=20, required=False)
    inv_no = forms.CharField(label='INV Number', max_length=20, required=False)
    status = forms.CharField(label='status', max_length=1, required=False)
    department = forms.CharField(label='Department', max_length=100, required=False)




