from distutils import config
from django.shortcuts import render,redirect
from django.http import HttpResponse,JsonResponse
from django.template import loader
# from financeapp.form import RegForm
from django.contrib.auth.models import User, auth
from django.contrib import messages
from .models import Registration
from .models import admin_add
import psycopg2
from django.core.mail import send_mail
from financeapp_project import settings
from financeapp import conn
from django.contrib import sessions 
from django.contrib.sessions.backends import signed_cookies
from django.contrib.auth import logout
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from .models import Department, Category, PO, SubCategory, IVR
from .form import *
from django.http import HttpResponse, HttpResponseRedirect
import datetime
import mimetypes
from os.path import basename
import random 
import string
from django.db.models import F,Q
from os.path import basename
import random, string, mimetypes, datetime, json
import pandas as pd
# Create your views here.
con=conn.conn
cur=con.cursor()
def get_random_password():
    # With combination of lower and upper case
    result_str = ''.join(random.choice(string.ascii_letters) for i in range(6))
    # print random string
    return result_str
def home(request):
    return HttpResponse("Hello, Django!")


def login(request):
    user_data=Employee.objects.all().order_by('id')
    email=""
    role=""
    password=""
    name=""
    first_name=""
    last_name=""
    depatment=""
    error_message=[]
    if request.method=='POST':
        # role=request.POST['Role']
        email=request.POST['email']
        password=request.POST['Password']
    
        isuser=False
        print(email)
        print(password)
        for data in user_data:
            if (email==data.email and password==data.temp_password):

                isuser=True
                role=data.role
                email=data.email
                name=data.first_name +" "+data.last_name 
                first_name=data.first_name
                last_name=data.last_name
                department=data.department

    
        if(isuser==True and role=='Administrator'):
            request.session['loggeduser']=name
            request.session['email']=email
            request.session['first_name']=first_name
            request.session['last_name']=last_name
            request.session['department']=department
            return redirect("admin_dashboard")
        elif(isuser==True and role=="President"):
            request.session['loggeduser']=name
            request.session['email']=email
            request.session['first_name']=first_name
            request.session['last_name']=last_name
            request.session['department']=department
            return redirect("president_dashboard")
        elif(isuser==True and role=="Head of Department"):
            request.session['loggeduser']=name
            request.session['email']=email
            request.session['first_name']=first_name
            request.session['last_name']=last_name
            request.session['department']=department
            return redirect("hod_view")
        elif(isuser==True and role=="Staff"):
            request.session['loggeduser']=name
            request.session['email']=email
            request.session['first_name']=first_name
            request.session['last_name']=last_name
            request.session['department']=department
            return redirect("staff_dashboard")
        elif(isuser==True and role=="Head of Finance"):
            request.session['loggeduser']=name
            request.session['email']=email
            request.session['first_name']=first_name
            request.session['last_name']=last_name
            request.session['department']=department
            return redirect("hof_dashboard")
        elif(isuser==True and role=="Finance Team"):
            request.session['loggeduser']=name
            request.session['email']=email
            request.session['first_name']=first_name
            request.session['last_name']=last_name
            request.session['department']=department
            return redirect("ft_view")
        else:
            messages.success(request,'Invalid username or password')
    
        
    
    return render(request,"login.html")

def log_out(request):
    del request.session['loggeduser']
    logout(request)
    return redirect('login')
def forgotpassword(request):
    user_data=Employee.objects.all().order_by('id')
    if request.method=='POST':
          email=request.POST['email']

          email_present=False
          for data in user_data:
                if(email==data.email):
                    email_present=True

                if(email_present):
                    random_password=get_random_password()
                    cur.execute('UPDATE financeapp_registration SET temp_password=%s where email=%s',(random_password,email))
                    con.commit()
                    message=f'Please log in to your account using this temporary password and reset your password.\nTemporay Password = {random_password}'
                    send_mail(
                        'Password Reset Procedure',
                        message,
                        settings.EMAIL_HOST_USER,
                        [email],
                        fail_silently=False,
                    )
                    return redirect("login/")
    return render(request,"reset_password.html")



def admin_add1(request):
    
   if "loggeduser" not in request.session:
        return redirect('login')
   else:
     if request.method == 'POST':
        fname=request.POST['fname']
        mname=request.POST['mname']
        lname=request.POST['lname']
        # name=request.POST['name']
        # temp_password=request.POST['temp_password']
        name=fname+" "+mname+" "+lname
        temp_password=get_random_password()
        email=request.POST['email']
        empID=request.POST['EmpID']
        role=request.POST['role']
        department=request.POST['department']
        if(len(empID)>0 and role != ""):
            cur.execute("INSERT INTO financeapp_admin_add (name,email,employeeid,role,department,temp_password)  VALUES(%s,%s,%s,%s,%s,%s)",(name,email,empID,role,department,temp_password))
            con.commit()
            
            message=f'Hello {name} you have been registed to the finance app.\n Your email is {email} and password is {temp_password}. \n Please login into your account to reset the password.'
            print("Registered")
            send_mail(
                'You have been registered',
                message,
                settings.EMAIL_HOST_USER,
                [email],
                fail_silently=False,
            )
        print(email)
   

     return render(request,"admin_add.html") 

def afterlogin(request):
    return render(request, 'home.html')

def presidentHome(request):
    username=request.session['loggeduser']
    print(username)
    return render(request,"president.html")
def hodHome(request):
    if "loggeduser" not in request.session:
        return redirect('login')
    else:
         return render(request,"hod_view.html")
def fteamHome(request):
    return render(request,"fteam.html")
def hofHome(request):
    return render(request,"hof.html")
def staffHome(request):
    if "loggeduser" not in request.session:
        return redirect('login')
    else:
         return render(request,"staff_dashboard.html")
# def profile(request):
#     return render(request,"profile.html")

def admin_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"admin_add(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})
def admin_dashboard_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"admin_dashboard(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})
def admin_edit_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"admin_edit(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})
def staff_dashboard_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"staff_dashboard(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})
def hod_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"hod_view(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})

def hod_submit_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"hod_submit(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})
    
def hod_approval_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"hod_approval(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})

def finanace_team_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"profile finance team.html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})

def staff_submit_po_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"staff_submit_po(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})

def hof_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"hof_profile.html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})

def staff_submit_ivr_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"staff_submit_ivr(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})

def president_dashboard_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"president_dashboard(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})

def president_ivr_for_approval_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"president_IVR_for_approval(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname})

def president_po_for_approval_profile(request):
    user_data=Employee.objects.all().order_by('id')
    username=request.session['loggeduser']
    firstname=""
    secondname=""
    role=""
    department=""
    empID=""
    email=""
    for data in user_data:
        if(username==(data.first_name +" "+data.last_name)):
            firstname=data.first_name
            secondname=data.last_name
            role=data.role
            department=data.department
            empID=data.employee_id
            email=data.email
    if request.method=="POST":
        old_password=request.POST['old_password']
        new_password=request.POST['new_password']
    
        for data in user_data:
            if(username==(data.first_name+" "+ data.last_name) and old_password==data.temp_password):
                cur.execute('UPDATE financeapp_employee SET temp_password=%s where employee_id=%s',(new_password,empID))
                con.commit()
    return render(request,"president_PO_for_approval(profile).html",{'name':username,'role':role,'department':department,'empID':empID,'email':email,'firstname':firstname,'secondname':secondname}) 
# def profile(request):
#     user_data=admin_add.objects.all().order_by('id')
#     username=request.session['loggeduser']
#     if request.method=="POST":
#         old_password=request.POST['old_password']
#         new_password=request.POST['new_password']
    
#         for data in user_data:
#             if(username==data.name and old_password==data.temp_password):
#                 cur.execute('UPDATE financeapp_admin_add SET temp_password=%s where name=%s',(new_password,username))
#                 con.commit()
#     return render(request,"admin_add(profile).html")
def admin_dashboard(request):
    if "loggeduser" not in request.session and "email" not in request.session:
        return redirect('login')
    else:
        user_data=Employee.objects.all().order_by('id')

        data={
            'user_data':user_data
        }
        user_count=len(user_data)
        hod=[]
        
        for data in user_data:
            if(data.role=='Head of Department'):
                hod.append(data)
        hod_count=len(hod)
        fteam=[]

        for data in user_data:
            if(data.role=='Finance Team'):
                fteam.append(data)
        fteam_count=len(fteam)
        staff=[]
        for data in user_data:
            if(data.role=='Staff'):
                staff.append(data) 
        staff_count=len(staff)
        president=[]
        
        for data in user_data:
            if(data.role=='President'):
                president.append(data)
        hof=[]
        
        for data in user_data:
            if(data.role=='Head of Finance'):
                hof.append(data)
    
        return render(request, "admin_dashboard.html",{'data':user_data,'hod_data':hod,'fteam_data':fteam,'staff_data':staff,'president_data':president,'hof_data':hof,
        'user_count':user_count,'hod_count':hod_count,'fteam_count':fteam_count,'staff_count':staff_count})

year = datetime.date.today().strftime('%Y')



def download_file(request, pno):

    document = get_object_or_404(PO, po_no=pno)
    file = document.attachment 
    #dowload the file w original file name pass arg to urls.py
    filename = basename(file.name)
    filename = filename
    #mimetypes will enable the file to be downloaded with extension
    content_type, _ = mimetypes.guess_type(file.name)
    response = HttpResponse(file, content_type=content_type)
    # response = HttpResponse(file, content_type='application/force-download')
    # response = HttpResponse(file, content_type='application/octet-stream')
    response['Content-Disposition'] = 'attachment: filename="{}"'.format(file.name)

    return response

#hod backend
def hod_view(request):
    return render(request, 'hod_view.html')

def hod_po_submitted_list(request):
    pos = PO.objects.order_by('po_no').all()
    if request.method == 'POST':
        return search_po_sort_by_status(request, 'hod_po_submitted_list.html',pos)
    else:
        return render(request, 'hod_po_submitted_list.html',{'pos':pos})       

def hod_ivr_submitted_list(request):
    ivrs = IVR.objects.order_by('inv_no').all()
    if request.method == 'POST':
        return search_ivr_sort_by_status(request,'hod_ivr_submitted_list.html',ivrs)
    else:
            return render(request, 'hod_ivr_submitted_list.html',{'ivrs':ivrs})

def hod_ivr(request):
    pos = PO.objects.order_by('po_no').filter(approval__status='a',approval__po_no=F(('po_no')))

    invno = get_invno()

    if request.method == 'POST':
        submit_ivr(request)
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    else:
        form = IVR_FORM()
        return render(request, 'hod_ivr.html', { 'form':form, 'pos':pos, 'invno':invno, }) 

def hod_po(request):
    departments = Department.objects.all()
    categories = Category.objects.all()
    sub_categories = SubCategory.objects.all()

    pno = get_pno()
    
    if request.method == 'POST':
        submit_po(request)
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
                    
    else:
        form = PO_Form()
        return render(request, 'hod_po.html', 
        { 'form':form, 'departments':departments,
         'categories':categories, 'sub_categories':sub_categories, 'pno':pno })

def hod_submit(request):
    return render(request, 'hod_submit.html')

def hod_approval(request):
    pos = PO.objects.order_by('po_no').filter(Q(approval__status='-') & Q(department=request.session['department'])).filter(approval__po_no=F(('po_no')))
    form = APPROVAL_FORM()

    if request.method == 'POST':
        form = APPROVAL_FORM(request.POST)
        search_form = SEARCH_FORM(request.POST)

        if request.POST.get('form_name') == 'r_form':
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Rejected.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

        if request.POST.get('form_name') == 'p_form':
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Sent For Approval.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
        
        if request.POST.get('form_name') == 'search':
            return search_po_sort_by_status(request, 'hod_approval.html', pos)

        else:
            return render(request, 'hod_approval.html', {'form':form, 'pos': pos})
    else:
        return render(request, 'hod_approval.html', {'form':form, 'pos': pos})

#admin dashboard backend
# def admin_dashboard(request):
#     return render(request, 'admin_dashboard.html')

def admin_add2(request):
    categories = Category.objects.all()
    departments = Department.objects.all()

    if request.method == 'POST':

        if request.POST.get('form_name') == 'catform':
            category_form = CATEGORY_FORM(request.POST)
            # print(form.errors)
            if category_form.is_valid():
                category_form.save()
                messages.success(request, 'Category Added Successful.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
                
        elif request.POST.get('form_name') == 'subcatform':
            subcategory_form = SUB_CATEGORY_FORM(request.POST)
            print('hello this is subcatform')
            # print(form.errors)
            if subcategory_form.is_valid():
                subcategory_form.save()
                messages.success(request, 'Sub Category Added Successful.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
        
        elif request.POST.get('form_name') == 'departform':
            department_form = DEPARTMENT_FORM(request.POST)
            # print(form.errors)
            if department_form.is_valid():
                department_form.save()
                messages.success(request, 'Department Added Successful.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
          
        elif request.POST.get('form_name') == 'userform':
            employee_form = EMPLOYEE_FORM(request.POST)
            
            print(employee_form.errors)
            if employee_form.is_valid():
                employee_form.save()
                name=request.POST['first_name']
                email=request.POST['email']
                temp_password=request.POST['temp_password']
                messages.success(request, 'Employee Added Successful.')
                message=f'Hello {name} you have been registed to the finance app.\n Your email is {email} and password is {temp_password}. \n Please login into your account to reset the password.'
                print("Registered")
                send_mail(
                'You have been registered',
                message,
                settings.EMAIL_HOST_USER,
                [email],
                fail_silently=False,
            )
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

    else:
        category_form = CATEGORY_FORM()
        subcategory_form = SUB_CATEGORY_FORM()
        department_form = DEPARTMENT_FORM()
        employee_form = EMPLOYEE_FORM()
        context = {
            'category_form':category_form,
            'subcategory_form':subcategory_form,
            'department_form':department_form,
            'employee_form':employee_form,
            'temp_password':get_random_password(),
            'categories':categories,
            'departments':departments
        }
        return render(request, 'admin_add.html', context)

def admin_edit(request):
    return render(request, 'admin_edit.html')

def admin_edit_cat(request):
    categories = Category.objects.order_by('id').all()

    if request.method == 'POST':
        form = CATEGORY_DELETE_FORM(request.POST)
        category_form = CATEGORY_FORM(request.POST)

        if request.POST.get('form_name') == 'deleteForm':
            if form.is_valid():
                id = request.POST.get('id')
                obj = get_object_or_404(Category,id=id)
                obj.delete()
                messages.success(request, 'Entry Deleted Successful.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
                
        elif request.POST.get('form_name') == 'catform':
            if category_form.is_valid():
                id = request.POST.get('id')
                obj = Category.objects.get(id=id)
                category_form = CATEGORY_FORM(request.POST, instance=obj)
                category_form.save()
                messages.success(request, 'Category Updated Successful.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

    else:
        form = CATEGORY_DELETE_FORM()
        category_form = CATEGORY_FORM()
        return render(request, 'admin_edit_cat.html', 
        {'form':form,'category_form':category_form, 'categories': categories}
        )

def admin_edit_subcat(request):
    categories = Category.objects.order_by('id').all()
    subcategories = SubCategory.objects.order_by('id').all()

    if request.method == 'POST':
        form = SUBCATEGORY_DELETE_FORM(request.POST)
        subcategory_form = SUB_CATEGORY_FORM(request.POST)

        if request.POST.get('form_name') == 'deleteForm':
            if form.is_valid():
                id = request.POST.get('id')
                obj = get_object_or_404(SubCategory,id=id)
                obj.delete()
                messages.success(request, 'Entry deleted Successful.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
        
        elif request.POST.get('form_name') == 'subcatform':
            if subcategory_form.is_valid():
                id = request.POST.get('id')
                obj = SubCategory.objects.get(id=id)
                subcategory_form = SUB_CATEGORY_FORM(request.POST, instance=obj)
                subcategory_form.save()
                messages.success(request, 'Sub-Category Updated Successful.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

    else:
        form =  SUBCATEGORY_DELETE_FORM()
        subcategory_form = SUB_CATEGORY_FORM()
        return render(request, 'admin_edit_subcat.html', 
        {'form':form,'subcategory_form':subcategory_form,'categories':categories,'subcategories':subcategories})

def admin_edit_user(request):
    employees = Employee.objects.order_by('id').all()
    departments = Department.objects.order_by('id').all()

    if request.method == 'POST':
        form = EMPLOYEE_DELETE_FORM(request.POST)
        employee_form = EMPLOYEE_FORM(request.POST)

        if request.POST.get('form_name') == 'deleteForm':
            if form.is_valid():
                id = request.POST.get('id')
                obj = get_object_or_404(Employee,id=id)
                obj.delete()
                messages.success(request, 'Entry deleted Successful.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
               messages.error(request, 'Invalid Data!')
            return HttpResponseRedirect(request.META['HTTP_REFERER'])

        elif request.POST.get('form_name') == 'userform':
            print(employee_form.errors)
            if employee_form.is_valid():
                id = request.POST.get('id')
                obj = Employee.objects.get(id=id)
                employee_form = EMPLOYEE_FORM(request.POST, instance=obj)
                employee_form.save()
                messages.success(request, 'Employee Updated Successful.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

    else:
        form =  EMPLOYEE_DELETE_FORM()
        employee_form = EMPLOYEE_FORM()
        return render(request, 'admin_edit_user.html', 
        {'form':form,'employee_form':employee_form,'departments':departments,'employees':employees})

def admin_edit_department(request):
    departments = Department.objects.order_by('id').all()

    if request.method == 'POST':
        form = DEPARTMENT_DELETE_FORM(request.POST)
        department_form = DEPARTMENT_FORM(request.POST)

        if request.POST.get('form_name') == 'deleteForm':
            if form.is_valid():
                id = request.POST.get('id')
                obj = get_object_or_404(Department,id=id)
                obj.delete()
                messages.success(request, 'Entry deleted Successful.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

        elif request.POST.get('form_name') == 'departmentform':
            if form.is_valid():
                id = request.POST.get('id')
                obj = Department.objects.get(id=id)
                department_form = DEPARTMENT_FORM(request.POST, instance=obj)
                department_form.save()
                messages.success(request, 'Department Updated Successful.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

    else:
        form =  SUBCATEGORY_DELETE_FORM()
        department_form = DEPARTMENT_FORM()
        return render(request, 'admin_edit_depart.html', 
        {'form':form, 'department_form':department_form,'departments':departments})

def admin_dashboard(request):
    user_data=Employee.objects.all().order_by('id')

    data={
        'user_data':user_data
    }
    user_count=len(user_data)
    hod=[]
    
    for data in user_data:
        if(data.role=='Head of Department'):
            hod.append(data)
    hod_count=len(hod)
    fteam=[]

    for data in user_data:
        if(data.role=='Finance Team'):
            fteam.append(data)
    fteam_count=len(fteam)
    staff=[]
    for data in user_data:
        if(data.role=='Staff'):
            staff.append(data) 
    staff_count=len(staff)
    president=[]
     
    for data in user_data:
        if(data.role=='President'):
            president.append(data)
    president_count = len(president)
    hof=[]
     
    for data in user_data:
        if(data.role=='Head of Finance'):
            hof.append(data)
    hof_count = len(hof)
   
    return render(request, "admin_dashboard.html",{'data':user_data,'hod_data':hod,'fteam_data':fteam,'staff_data':staff,'president_data':president,'hof_data':hof,
    'user_count':user_count, 'president_count':president_count, 'hof_count':hof_count, 'hod_count':hod_count,'fteam_count':fteam_count,'staff_count':staff_count})

#staff backend
def staff_dashboard(request):
    return render(request,'staff_dashboard.html')

def staff_po_list(request):
    pos = PO.objects.order_by('po_no').all()
    if request.method == 'POST':
        return search_po_sort_by_status(request,'staff_po_list.html',pos)
    return render(request,'staff_po_list.html', {'pos':pos})

def staff_ivr_list(request):
    ivrs = IVR.objects.order_by('inv_no').all()
    if request.method == 'POST':
        return search_ivr_sort_by_status(request,'staff_ivr_list.html',ivrs)
    return render(request, 'staff_ivr_list.html',{'ivrs':ivrs})

def staff_submit_po(request):
    departments = Department.objects.all()
    categories = Category.objects.all()
    sub_categories = SubCategory.objects.all()

    pno = get_pno()
    
    if request.method == 'POST':
        submit_po(request)
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
                
    else:
        form = PO_Form()
        return render(request, 'staff_submit_po.html', 
        { 'form':form, 'departments':departments,
         'categories':categories, 'sub_categories':sub_categories, 'pno':pno })

def staff_submit_ivr(request):
    pos = PO.objects.order_by('po_no').filter(approval__status='a',approval__po_no=F(('po_no')))

    invno = get_invno()

    if request.method == 'POST':
        submit_ivr(request)
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    else:
        form = IVR_FORM()
        return render(request, 'staff_submit_ivr.html', { 'form':form, 'pos':pos, 'invno':invno, }) 
    
#president  
def president_dashboard(request):
    return render(request,'president_dashboard.html')

def president_dashboard_po(request):
    pos = PO.objects.order_by('po_no').all()
    departments = Department.objects.all()
    if request.method == 'POST':
        if request.POST.get('form_name') == 'search' or request.POST.get('form_name') == 'sort':
            return search_po_sort_by_status(request, 'president_dashboard_po.html',pos)
        elif request.POST.get('form_name') == 'sort_department':
            return sort_by_department(request, 'president_dashboard_po.html', pos, departments)
    else:
        return render(request, 'president_dashboard_po.html',{'pos':pos, 'departments':departments})

def president_dashboard_ivr(request):
    ivrs = IVR.objects.order_by('inv_no').all()
    departments = Department.objects.all()
    if request.method == 'POST':
        if request.POST.get('form_name') == 'search' or request.POST.get('form_name') == 'sort':
            return search_ivr_sort_by_status(request, 'president_dashboard_ivr.html',ivrs)
        elif request.POST.get('form_name') == 'sort_department':
            return sort_by_department_ivr(request, 'president_dashboard_ivr.html', ivrs, departments)
    else:
        return render(request, 'president_dashboard_ivr.html',{'ivrs':ivrs, 'departments':departments})

def president_po_for_approval(request):
    departments = Department.objects.all()
    pos = PO.objects.order_by('po_no').filter(approval__status='p',amount__gt=100000).filter(approval__po_no=F(('po_no')))

    if request.method == 'POST':
        form = APPROVAL_FORM(request.POST)

        if request.POST.get('form_name') == 'r_form':
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Rejected.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

        elif request.POST.get('form_name') == 'a_form':
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Approved.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

        elif request.POST.get('form_name') == 'sort_department':
            return sort_by_department(request, 'president_PO_for_approval.html', pos, departments)
    
        elif request.POST.get('form_name') == 'search':
            return search_po_sort_by_status(request, 'president_PO_for_approval.html',pos)

        else:
            return render(request, 'president_PO_for_approval.html', {'pos': pos, 'departments':departments})

    else:
        return render(request, 'president_PO_for_approval.html', {'pos': pos, 'departments':departments})

def president_ivr_for_approval(request):
    ivrs = IVR.objects.order_by('inv_no').filter(approval2__status='p',amount__gt=100000).filter(approval2__inv_no=F(('inv_no')))
    departments = Department.objects.all()

    if request.method == 'POST':
        form = APPROVAL_FORM2(request.POST)

        if request.POST.get('form_name') == 'r_form':
            print(form.errors)
            if form.is_valid():
                save_status_form_ivr(request, Approval2, APPROVAL_FORM2)
                messages.success(request, 'IVR Rejected.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

        if request.POST.get('form_name') == 'a_form':
            print(form.errors)
            if form.is_valid():
                save_status_form_ivr(request, Approval2, APPROVAL_FORM2)
                messages.success(request, 'IVR Approved.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
                    
        if request.POST.get('form_name') == 'search':
            return search_ivr_sort_by_status(request,'president_IVR_for_approval.html',ivrs)

        if request.POST.get('form_name') == 'sort_department':
            return sort_by_department_ivr(request, 'president_IVR_for_approval.html', ivrs, departments)
    else:
        return render(request, 'president_IVR_for_approval.html', {'ivrs': ivrs, 'departments':departments})

def get_random_password():
    result_str = ''.join(random.choice(string.ascii_letters) for i in range(6))
    return result_str

#ft
def ft_view(request):
    return render(request, 'ft_view.html')

def ft_po_submitted_list(request):
    pos = PO.objects.order_by('po_no').all()
    if request.method == 'POST':
        return search_po_sort_by_status(request,'ft_po_submitted_list.html',pos)
    return render(request, 'ft_po_submitted_list.html',{'pos':pos})

def ft_ivr_submitted_list(request):
    ivrs = IVR.objects.order_by('inv_no').all()
    if request.method == 'POST':
        return search_ivr_sort_by_status(request,'ft_ivr_submitted_list.html',ivrs)
    return render(request, 'ft_ivr_submitted_list.html',{'ivrs':ivrs})

def ft_ivr(request):
    pos = PO.objects.order_by('po_no').filter(approval__status='a',approval__po_no=F(('po_no')))

    invno = get_invno()

    if request.method == 'POST':
        submit_ivr(request)
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
    else:
        form = IVR_FORM()
        return render(request, 'ft_ivr.html', 
        { 'form':form, 'pos':pos, 'invno':invno, }) 

def ft_po(request):
    departments = Department.objects.all()
    categories = Category.objects.all()
    sub_categories = SubCategory.objects.all()

    pno = get_pno()
    
    if request.method == 'POST':
        submit_po(request)
        return HttpResponseRedirect(request.META['HTTP_REFERER'])
                
    else:
        form = PO_Form()
        return render(request, 'ft_po.html', 
        { 'form':form, 'departments':departments,
         'categories':categories, 'sub_categories':sub_categories, 'pno':pno })

def ft_submit(request):
    return render(request, 'ft_submit.html')

def ft_approval(request):
    pos = PO.objects.order_by('po_no').filter(approval__status='-',approval__po_no=F(('po_no')))
    departments = Department.objects.all()
    form = APPROVAL_FORM()

    if request.method == 'POST':
        form = APPROVAL_FORM(request.POST)
        search_form = SEARCH_FORM(request.POST)

        if request.POST.get('form_name') == 'r_form':
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Rejected.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

        elif request.POST.get('form_name') == 'p_form':
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Sent For Approval.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                messages.error(request, 'Invalid Data!')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

        elif request.POST.get('form_name') == 'sort_department':
            return sort_by_department(request, 'ft_approval.html', pos, departments)

        elif request.POST.get('form_name') == 'search':
            return search_po_sort_by_status(request, 'ft_approval.html',pos)
            
    else:
        return render(request, 'ft_approval.html', {'form':form, 'pos': pos, 'departments':departments})

#hof
def hof_dashboard(request):
    return render(request,'hof_dashboard.html')

def hof_dashboard_ivr(request):
    ivrs = IVR.objects.order_by('inv_no').filter(amount__gt=100000).filter(Q(approval2__status='r') | Q(approval2__status='a')).filter(approval2__inv_no=F(('inv_no')))
    if request.method == 'POST':
        return search_ivr_sort_by_status(request, 'hof_dashboard_IVR.html',ivrs)
    return render(request, 'hof_dashboard_IVR.html',{'ivrs':ivrs})

def hof_dashboard_po(request):
    pos = PO.objects.order_by('po_no').filter(amount__gt=100000).filter(Q(approval__status='r') | Q(approval__status='a')).filter(approval__po_no=F(('po_no')))
    if request.method == 'POST':
        return search_po_sort_by_status(request, 'hof_dashboard_PO.html',pos)
    return render(request, 'hof_dashboard_PO.html',{'pos':pos})

def hof_ivr_for_approval(request):
    ivrs = IVR.objects.order_by('inv_no').filter(approval2__status='p',amount__lt=100000).filter(approval2__inv_no=F(('inv_no')))
    form = APPROVAL_FORM2()
    departments = Department.objects.all()

    if request.method == 'POST':
        form = APPROVAL_FORM2(request.POST)

        if request.POST.get('form_name') == 'r_form':
            print(form.errors)
            if form.is_valid():
                save_status_form_ivr(request, Approval2, APPROVAL_FORM2)
                messages.success(request, 'IVR Rejected.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                return render(request, 'hof_IVR_for_approval.html', {'form':form, 'ivrs': ivrs, 'departments':departments})

        elif request.POST.get('form_name') == 'a_form':
            print(form.errors)
            if form.is_valid():
                save_status_form_ivr(request, Approval2, APPROVAL_FORM2)
                messages.success(request,'IVR Approved.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                return render(request, 'hof_IVR_for_approval.html', {'form':form, 'ivrs': ivrs, 'departments':departments})

        elif request.POST.get('form_name') == 'search':
            return search_ivr_sort_by_status(request, 'hof_IVR_for_approval.html',ivrs)

        elif request.POST.get('form_name') == 'sort_department':
            return sort_by_department_ivr(request, 'hof_IVR_for_approval.html', ivrs, departments)
 
    else:
        return render(request, 'hof_IVR_for_approval.html', {'form':form, 'ivrs': ivrs, 'departments':departments})

def hof_po_for_approval(request):
    pos = PO.objects.order_by('po_no').filter(approval__status='p', amount__lt=100000).filter(approval__po_no=F(('po_no')))
    form = APPROVAL_FORM()
    departments = Department.objects.all()

    if request.method == 'POST':
        form = APPROVAL_FORM(request.POST)

        if request.POST.get('form_name') == 'r_form':
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Rejected.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                return render(request, 'hod_approval.html', {'form':form, 'pos': pos})

        elif request.POST.get('form_name') == 'a_form':
            print(form.errors)
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Approved.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                return render(request, 'hof_PO_for_approval.html', {'form':form, 'pos': pos})

        elif request.POST.get('form_name') == 'search':
            return search_po_sort_by_status(request, 'hof_PO_for_approval.html',pos)
        
        elif request.POST.get('form_name') == 'sort_department':
            return sort_by_department(request, 'hof_PO_for_approval.html', pos, departments)

    else:
        return render(request, 'hof_PO_for_approval.html', {'form':form, 'pos': pos, 'departments':departments})

def hof_pending_forms(request):
    return render(request,'hof_pending_forms.html')

def hof_pending_ivr(request):
    ivrs = IVR.objects.order_by('inv_no').filter(approval2__status='b',amount__gt=300000).filter(approval2__inv_no=F(('inv_no')))
    departments = Department.objects.all()

    if request.method == 'POST':
        form = APPROVAL_FORM2(request.POST)

        if request.POST.get('form_name') == 'r_form':
            print(form.errors)
            if form.is_valid():
                save_status_form_ivr(request, Approval2, APPROVAL_FORM2)
                messages.success(request, 'IVR Rejected.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                return render(request, 'hof_pending_IVR.html', {'form':form, 'ivrs': ivrs, 'departments':departments})

        elif request.POST.get('form_name') == 'a_form':
            print(form.errors)
            if form.is_valid():
                save_status_form_ivr(request, Approval2, APPROVAL_FORM2)
                messages.success(request,'IVR Approved.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                return render(request, 'hof_pending_IVR.html', {'form':form, 'ivrs': ivrs, 'departments':departments})

        elif request.POST.get('form_name') == 'search':
            return search_ivr_sort_by_status(request, 'hof_pending_IVR.html',ivrs)

        elif request.POST.get('form_name') == 'sort_department':
            return sort_by_department_ivr(request, 'hof_pending_IVR.html', ivrs, departments)
    return render(request, 'hof_pending_IVR.html',{'ivrs':ivrs, 'departments':departments})

def hof_approval(request):
    pos = PO.objects.order_by('po_no').filter(Q(approval__status='-') & Q(department=request.session['department'])).filter(approval__po_no=F(('po_no')))
    form = APPROVAL_FORM()

    if request.method == 'POST':
        form = APPROVAL_FORM(request.POST)

        if request.POST.get('form_name') == 'r_form':
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Rejected.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])

        if request.POST.get('form_name') == 'p_form':
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Sent For Approval.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
        
        if request.POST.get('form_name') == 'search':
            return search_po_sort_by_status(request, 'hof_approval.html', pos)

    else:
        return render(request, 'hof_approval.html', {'form':form, 'pos': pos})

def hof_pending_po(request):
    pos = PO.objects.order_by('po_no').filter(approval__status='b', amount__gt=300000).filter(approval__po_no=F(('po_no')))
    departments = Department.objects.all()
    if request.method == 'POST':
        form = APPROVAL_FORM(request.POST)

        if request.POST.get('form_name') == 'r_form':
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Rejected.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                return render(request, 'hod_approval.html', {'form':form, 'pos': pos})

        elif request.POST.get('form_name') == 'a_form':
            print(form.errors)
            if form.is_valid():
                save_status_form_po(request, Approval, APPROVAL_FORM)
                messages.success(request, 'PO Approved.')
                return HttpResponseRedirect(request.META['HTTP_REFERER'])
            else:
                return render(request, 'hof_pending_PO.html', {'pos': pos})

        elif request.POST.get('form_name') == 'search':
            return search_po_sort_by_status(request, 'hof_pending_PO.html',pos)
        
        elif request.POST.get('form_name') == 'sort_department':
            return sort_by_department(request, 'hof_pending_PO.html', pos, departments)

    return render(request, 'hof_pending_PO.html',{'pos':pos, 'departments':departments})

####
year = datetime.date.today().strftime('%Y')

def get_pno():
    largest = PO.objects.all().order_by('po_no').last()
    if not largest:
        pid = f'{1:05}'
        return 'PO'+year+pid

    else:
        lpo = largest.po_no
        po = 'PO'+year
        lpo = int(lpo.replace(po,'').lstrip())+1
        pid = f'{lpo:05}'
        return 'PO'+year+pid

def get_invno():
    largest = IVR.objects.all().order_by('inv_no').last()
    if not largest:
        inid = f'{1:05}'
        return 'IVR'+year+inid

    else:
        lpo = largest.inv_no
        po = 'IVR'+year
        lpo = int(lpo.replace(po,'').lstrip())+1
        inid = f'{lpo:05}'
        return 'IVR'+year+inid

def submit_po(request):
    form = PO_Form(request.POST, request.FILES)
    print(form.errors)
    if form.is_valid():
        form.save(request=request)
        messages.success(request, 'Form Submission Successful.')
    else:
        messages.success(request, 'Form Field Invalid! Try again.')

def submit_ivr(request):
    form = IVR_FORM(request.POST, request.FILES)
    print(form.errors)
    if form.is_valid():
       
        form.save(request=request)
        messages.success(request, 'Form submission Successful.')
    else:
        messages.success(request, 'Form Field Invalid! Try again.')

def search_po_sort_by_status(request,html_template,pos):
    form = SEARCH_FORM(request.POST)
    departments = Department.objects.all()

    if request.POST.get('form_name') == 'search':
        print(form.errors)
        if form.is_valid():
            po_no = form.cleaned_data['po_no']
            pos = pos.filter(po_no=po_no)
            return render(request,html_template,{'pos':pos, 'departments':departments})
        else:
            pos = None
            return render(request,html_template,{'pos':pos, 'departments':departments})

    elif request.POST.get('form_name') == 'sort':
        print(form.errors)
        if form.is_valid():
            status = form.cleaned_data['status']
            if status == '':
                pos = pos.all()
                return render(request, html_template,{'pos':pos, 'departments':departments})
            else:
                pos = pos.filter(approval__status=status,approval__po_no=F(('po_no')))
                return render(request, html_template,{'pos':pos, 'departments':departments})
        else:
            pos = None
            return render(request, html_template,{'pos':pos, 'departments':departments})
    
    else:
        return render(request, html_template,{'pos':pos, 'departments':departments})

def sort_by_department(request, html_template, pos, departments):
    form = SEARCH_FORM(request.POST)
    if request.POST.get('form_name') == 'sort_department':
        if form.is_valid():
            department = request.POST.get('department')
            if department == '':
                pos = pos.all()
                return render(request, html_template,{'pos':pos,'departments':departments})
            else:
                pos = pos.filter(department=department)
                return render(request, html_template,{'pos':pos,'departments':departments})
        else:
            pos = None
            return render(request, html_template,{'pos':pos,'departments':departments})

def sort_by_department_ivr(request, html_template, ivrs, departments):
    form = SEARCH_FORM(request.POST)
    if request.POST.get('form_name') == 'sort_department':
        if form.is_valid():
            department = request.POST.get('department')
            if department == '':
                ivrs = ivrs.all()
                return render(request, html_template,{'ivrs':ivrs,'departments':departments})
            else:
                ivrs = ivrs.filter(department=department)
                return render(request, html_template,{'ivrs':ivrs,'departments':departments})
        else:
            ivrs = None
            return render(request, html_template,{'ivrs':ivrs,'departments':departments})
    

def search_ivr_sort_by_status(request,html_template,ivrs):
    form = SEARCH_FORM(request.POST)
    departments = Department.objects.all()
    print(form.errors)
    if request.POST.get('form_name') == 'search':
        if form.is_valid():
            inv_no = form.cleaned_data['inv_no']
            ivrs = ivrs.filter(inv_no=inv_no)
            return render(request, html_template, {'ivrs':ivrs})
        else:
            ivrs = None
            print('this is none')
            return render(request, html_template, {'ivrs':ivrs})

    elif request.POST.get('form_name') == 'sort':
            print(form.errors)
            if form.is_valid():
                status = form.cleaned_data['status']
                if status == '':
                    ivrs = ivrs.all()
                    return render(request, html_template,{'ivrs':ivrs, 'departments':departments})
                else:
                    ivrs = ivrs.order_by('inv_no').filter(approval2__status=status,approval2__inv_no=F(('inv_no')))
                    return render(request, html_template, {'ivrs':ivrs, 'departments':departments})
            else:
                ivrs = None
                return render(request, html_template, {'ivrs':ivrs, 'departments':departments})

def save_status_form_po(request, model, form):
  
     # id = request.POST.get('id')
    # obj = model.objects.get(po_no=id)
    # form_ = form(request.POST, instance=obj)
    # form_.save(po_no=request.POST['po_no'], status=request.POST['status'])
    # form_.save()
    
    po_no = request.POST.get('id')
    status = request.POST.get('status')

    po_amount = PO.objects.get(po_no=po_no).amount
    instance = Approval.objects.get(po_no=po_no)
    email=PO.objects.get(po_no=po_no).email_submitted_by
    status_from_instance = instance.status

    if status_from_instance == 'p':
        if po_amount > 300000 and status == 'a':
            instance.status = 'b'
        elif po_amount > 300000 and status == 'r':
            instance.status = 'r'
        elif  po_amount <= 300000 and status == 'a':
            instance.status = 'a'
        elif po_amount > 300000 and status == 'r':
            instance.status = 'r'
    if status_from_instance == 'b':
        if po_amount > 300000 and status == 'a':
            instance.status = 'a'
        if po_amount > 300000 and status == 'r':
            instance.status = 'r'
    if status_from_instance == '-':
        if po_amount > 300000 and status == 'p':
            instance.status = 'p'
        elif po_amount > 300000 and status == 'r':
            instance.status = 'r'
        elif po_amount < 300000 and status == 'p':
            instance.status = 'p'
        elif po_amount < 300000 and status == 'r':
            instance.status = 'r'
    message=f'Please log in to your account to check the status of the {po_no} PO you submitted.'
    send_mail(
        'PO status update',
        message,
        settings.EMAIL_HOST_USER,
        [email],
        fail_silently=False,
    )
    Approval.objects.update_or_create(po_no=po_no, defaults={'status': instance.status})
    

def save_status_form_ivr(request, model, form):
   # id = request.POST.get('id')
    # obj = model.objects.get(inv_no=id)
    # form_ = form(request.POST, request=request, instance=obj)
    # form_.save(inv_no=request.POST['inv_no'], status=request.POST['status'])
    inv_no = request.POST.get('id')
    status = request.POST.get('status')

    po_amount = IVR.objects.get(po_no=inv_no).amount
    instance = Approval2.objects.get(po_no=inv_no)
    email=IVR.objects.get(po_no=inv_no).email_submitted_by
    status_from_instance = instance.status

    if status_from_instance == 'p':
        if po_amount > 300000 and status == 'a':
            instance.status = 'b'
        elif po_amount > 300000 and status == 'r':
            instance.status = 'r'
        elif  po_amount <= 300000 and status == 'a':
            instance.status = 'a'
        elif po_amount > 300000 and status == 'r':
            instance.status = 'r'
    if status_from_instance == 'b':
        if po_amount > 300000 and status == 'a':
            instance.status = 'a'
        if po_amount > 300000 and status == 'r':
            instance.status = 'r'
    if status_from_instance == '-':
        if po_amount > 300000 and status == 'p':
            instance.status = 'p'
        elif po_amount > 300000 and status == 'r':
            instance.status = 'r'
        elif po_amount < 300000 and status == 'p':
            instance.status = 'p'
        elif po_amount < 300000 and status == 'r':
            instance.status = 'r'
    message=f'Please log in to your account to check the status of the {inv_no} IVR you submitted.'
    send_mail(
        'PO status update',
        message,
        settings.EMAIL_HOST_USER,
        [email],
        fail_silently=False,
    )
    Approval2.objects.update_or_create(inv_no=inv_no, defaults={'status': instance.status})
# fn to export to excel
def export_to_excel(instance, file_name):
    try:
        # Convert the instance to a DataFrame
        data = list(instance.values())
        df = pd.DataFrame(data)

        if 'attachment' in df.columns:
            df = df.drop(columns=['attachment'])
        if 'name' in df.columns:
            df = df.drop(columns=['name'])

        df.columns = ['Date', 'PO Number', 'Category', 'Sub Category', 'Department', 'Head of Department', 'Currency', 'Currency Amount', 'Amount (NU)', 'Quantity',        
        'Description', 'Supplier', 'Email (Submitted By)']

        # Create a HttpResponse with the Excel file
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = f'attachment; filename="{file_name}.xlsx"'

        # Write the DataFrame to the response
        df.to_excel(response, index=False)
        return response
    except KeyError:
        print("Column 'attachment' not found in dataframe")
        return HttpResponse(content="An error occurred while exporting selected POs", status=500)

def export_po(request, po_no):
    instance = PO.objects.filter(po_no=po_no).values()
    response = export_to_excel(instance, po_no)
    return response

# export multiple pos
def export_to_excel_selected(instances, file_name):
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="{}.xlsx"'.format(file_name)

    df = pd.DataFrame.from_records(instances.values())

    if 'attachment' in df.columns:
            df = df.drop(columns=['attachment'])

    if 'name' in df.columns:
            df = df.drop(columns=['name'])

    df.columns = ['Date', 'PO Number', 'Category', 'Sub Category', 'Department', 'Head of Department', 'Currency', 'Currency Amount', 'Amount (NU)', 'Quantity',        
    'Description', 'Supplier Name', 'Email (Submitted By)']

    df.to_excel(response, sheet_name='Selected PO', index=False)

    return response

def export_selected_po(request):
    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        selected_po = body['selected_po']
        instances = PO.objects.filter(po_no__in=selected_po)
        response = export_to_excel_selected(instances, "selected_pos")
        return response

# exporting to excel
def export_to_excel_ivr(instance, file_name):
    try:
        # Convert the instance to a DataFrame
        data = list(instance.values())
        df = pd.DataFrame(data)

        if 'attachment' in df.columns:
            df = df.drop(columns=['attachment'])
        if 'name' in df.columns:
            df = df.drop(columns=['name'])

        df.columns = ['Date', 'IVR Number', 'PO Number', 'Category', 'Sub Category', 'Department', 'Head of Department', 'Currency', 'Currency Amount', 'Amount (NU)', 'Quantity',        
        'Description', 'Supplier Name', 'Email (Submitted By)', 'Payee Name', 'Payee Bank Name', 'Payee Account Number', 'TPN Number']

        # Create a HttpResponse with the Excel file
        response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = f'attachment; filename="{file_name}.xlsx"'

        # Write the DataFrame to the response
        df.to_excel(response, index=False)
        return response
    except KeyError:
        print("Column 'attachment' not found in dataframe")
        return HttpResponse(content="An error occurred while exporting selected POs", status=500)

def export_ivr(request, inv_no):
    instance = IVR.objects.filter(inv_no=inv_no).values()
    response = export_to_excel_ivr(instance, inv_no)
    return response

#exporting ivrs 
def export_to_excel_selected_ivr(instances, file_name):
    if not instances.exists():
        return HttpResponse("No data to export")
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename="{}.xlsx"'.format(file_name)

    df = pd.DataFrame.from_records(instances.values())

    if 'attachment' in df.columns:
            df = df.drop(columns=['attachment'])

    if 'name' in df.columns:
            df = df.drop(columns=['name'])

    df.columns = ['Date', 'IVR Number', 'PO Number', 'Category', 'Sub Category', 'Department', 'Head of Department', 'Currency', 'Currency Amount', 'Amount (NU)', 'Quantity',        
    'Description', 'Supplier Name', 'Email (Submitted By)', 'Payee Name', 'Payee Bank Name', 'Payee Account Number', 'TPN Number']

    df.to_excel(response, sheet_name='Selected IVR', index=False)

    return response

def export_selected_ivr(request):
    if request.method == 'POST':
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        selected_ivr = body['selected_po']
        instances = IVR.objects.filter(inv_no__in=selected_ivr)
        response = export_to_excel_selected_ivr(instances, "selected_ivrs")
        return response
def get_po_data(request, po_no):
    po = PO.objects.get(po_no=po_no)
    #using model to dict, need not explicitly define each field
    # po_data = model_to_dict(po)
    data = {
        'po_no':po.po_no,'date':po.date,'department':po.department,
        'category':po.category,'sub_category':po.sub_category,
        'amount_currency':po.amount_currency,'currency':po.currency, 'hod':po.hod,
        'amount':po.amount, 'supplier_name':po.supplier_name, 'description':po.description,
        'quantity':po.quantity,
    }
    return JsonResponse(data)


